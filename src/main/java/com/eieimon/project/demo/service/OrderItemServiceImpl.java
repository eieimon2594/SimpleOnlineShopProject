package com.eieimon.project.demo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.OrderItem;
import com.eieimon.project.demo.entity.Product;

@Service
public class OrderItemServiceImpl implements OrderItemService {
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    @Transactional
    public List<OrderItem> getOrderItemList() {
        Query query = entityManager.createQuery("SELECT o FROM OrderItem o");
        List results = query.getResultList();
        return results;
    }
    }



