package com.eieimon.project.demo.service;

import java.util.List;

import com.eieimon.project.demo.entity.Category;
import com.eieimon.project.demo.entity.Product;

public interface CategoryService {
    void save(Category category);

    List<Category> findAll();

    Category findById(Long id);

     List<Product> findByCategoryId(Long id);

}
