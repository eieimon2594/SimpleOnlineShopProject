package com.eieimon.project.demo.service;

import java.util.List;
import java.util.Set;

import com.eieimon.project.demo.entity.Order;
import com.eieimon.project.demo.entity.Product;

public interface ProductService {
    public void save(Product product);

    public List<Product> getProductList();

    public Product getProducts(Long id);

    public List<Product> getProductsbyCategory(Long id);

    List<Product> getProductsByIds(Set<Long> productIds);

    void saveOrder(Order order, Set<Long> productIds);

}
