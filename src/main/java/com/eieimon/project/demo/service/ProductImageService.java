package com.eieimon.project.demo.service;

import com.eieimon.project.demo.entity.ProductImage;

public interface ProductImageService {
    public void saveImagePath(ProductImage productImage);

    public ProductImage findById(Long id);

    public ProductImage findByProductId(long pid);

}
