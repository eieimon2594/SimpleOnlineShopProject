package com.eieimon.project.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.Category;
import com.eieimon.project.demo.entity.Order;
import com.eieimon.project.demo.entity.OrderItem;
import com.eieimon.project.demo.entity.Product;

@Service
public class ProductServiceImpl implements ProductService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Product product) {
        // TODO Auto-generated method stub
        if (product.getId() == null) {
            entityManager.persist(product);
        } else {
            entityManager.merge(product);
        }

    }

    @Override
    @Transactional
    public List<Product> getProductList() {
        Query query = entityManager.createQuery("SELECT p FROM Product p");
        List results = query.getResultList();
        return results;
    }

    @Override
    @Transactional
    public Product getProducts(Long id) {
        Query query = entityManager.createQuery("SELECT p FROM Product p WHERE p.id =:id");
        query.setParameter("id", id);
        Product product = (Product) query.getSingleResult();
        System.out.println(">>>>>>>>>>>>>>" + product.getId());
        System.out.println(">>>>>>>>>>>>>>" + product.getName());
        return product;
    }

    @Override
    @Transactional
    public List<Product> getProductsbyCategory(Long id) {
        Query query = entityManager.createQuery("SELECT p FROM Product p WHERE p.id=:id");
        query.setParameter("id", id);
        List<Product> productList = query.getResultList();
        return productList;
    }
    
    @Override
    public List<Product> getProductsByIds(Set<Long> productIds) {
        List<Product> products = new ArrayList<>();
        if (productIds == null) {
            return products;
        }
        
        for (Long productId : productIds) {
            Product product = this.entityManager.find(Product.class, productId);
            products.add(product);
        }
        return products;
    }

    @Override
    @Transactional
    public void saveOrder(Order order, Set<Long> productIds) {
        order.setStatus("waiting");
        order.setDate(new Date());
        
        List<OrderItem> orderItems = new ArrayList<>();
        for (Long productId : productIds) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrder(order);
            orderItem.setQuantity(1);
            orderItem.setProduct(this.entityManager.getReference(Product.class, productId));
            orderItems.add(orderItem);
        }
        order.setOrderItems(orderItems);
        
        this.entityManager.persist(order);
    }

}
