package com.eieimon.project.demo.service;

import java.util.List;

import com.eieimon.project.demo.entity.OrderItem;

public interface OrderItemService {
    List<OrderItem> getOrderItemList();

}
