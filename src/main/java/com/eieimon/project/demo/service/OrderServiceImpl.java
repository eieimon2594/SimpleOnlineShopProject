package com.eieimon.project.demo.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.Order;

@Service
public class OrderServiceImpl implements OrderService {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    @Transactional
    public void save(Order order) {
        if (order.getId() == null) {
            entityManager.persist(order);
        } else {
            entityManager.merge(order);
        }
            
    }

}
