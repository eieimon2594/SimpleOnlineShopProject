package com.eieimon.project.demo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.Category;
import com.eieimon.project.demo.entity.Product;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CategoryServiceImpl implements CategoryService {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    @Transactional
    public void save(Category category) {
        // TODO Auto-generated method stub
        if (category.getId() == null) {
            entityManager.persist(category);
        } else {
            entityManager.merge(category);
        }

    }

    @Override
    @Transactional
    public List<Category> findAll() {
        Query query = entityManager.createQuery("SELECT c FROM Category c", Category.class);
        List<Category> results = query.getResultList();
        return results;
    }

    @Override
    @Transactional
    public Category findById(Long id) {
        return entityManager.find(Category.class, id);
    }

    @Override
    @Transactional
    public List <Product> findByCategoryId(Long id) {
        Query query = entityManager.createQuery("SELECT p from Product p JOIN p.categories c WHERE c.id=:id", Product.class);
        query.setParameter("id", id);
        List<Product> products = query.getResultList();
        return products;
        
    }

}
