package com.eieimon.project.demo.service;

import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eieimon.project.demo.entity.User;

@Service
public class UserServiceImpl implements UserService {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(User user) {
        // TODO Auto-generated method stub
        if (user.getId() == null) {
            entityManager.persist(user);
        } else {
            entityManager.merge(user);

        }

    }

    @Override
    @Transactional
    public User findById(Long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    @Transactional
    public User findByEmail(String email) {
        // TODO Auto-generated method stub
        User user = null;
        System.out.println("Email >>>>>>>>>>" + email);
        try {
            Query query = entityManager.createQuery("select u from User u where u.email =:email");
            query.setParameter("email", email);
            user = (User) query.getSingleResult();

        } catch (Exception ex) {

        }
        if (user == null) {
            System.out.println("User >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        } else {
            System.out.println(">>>>>>>>>>>>>>>>>>>>" + user.getEmail());
            System.out.println(">>>>>>>>>>>>>>>>>>>>" + user.getPassword());
        }
        return user;
    }
}
