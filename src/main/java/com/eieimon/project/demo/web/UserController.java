package com.eieimon.project.demo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eieimon.project.demo.entity.User;
import com.eieimon.project.demo.service.UserService;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/login")
    public String showLoginForm() {
        return "login";
    }

    @GetMapping("/")
    // @ResponseBody
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
            Model model) {
        model.addAttribute("name", name);
        return "redirect:/products/create";
        // return "Hello " + name;
        // return "Login Success.";
    }

    @GetMapping("/signup")
    public String userSignup(Model model) {
        model.addAttribute("user", new User());
        return "signupForm";
    }

    @PostMapping("/signup")
    public String createUser(@Validated @ModelAttribute User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "signupForm";
        }
        String userpassword = user.getPassword();
        user.setPassword(passwordEncoder.encode(userpassword));
        user.setRole("user");
        userService.save(user);
        System.out.println(" " + user.getUsername());
        return "redirect:/login";
    }

}