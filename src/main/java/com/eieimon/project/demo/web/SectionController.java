package com.eieimon.project.demo.web;


import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.eieimon.project.demo.entity.Order;
import com.eieimon.project.demo.entity.Product;
import com.eieimon.project.demo.service.OrderItemService;
import com.eieimon.project.demo.service.OrderService;
import com.eieimon.project.demo.service.ProductService;

@SpringBootApplication
@ComponentScan("com")
@Controller
public class SectionController extends SpringBootServletInitializer {
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderItemService orderitemService;

    @GetMapping("/product/{id}/details")
    public String getAddtoCartPage(Model model, @PathVariable("id") Long id) {
        model.addAttribute("id", id);
        model.addAttribute("productdetails", productService.getProducts(id));
        return "productdetails";
    }
    /*@PostMapping(value = "/product/{id}/details")
    public String postAddtoCartPage(@ModelAttribute Product product,HttpServletRequest request, @PathVariable("id") Long id ) {
        Order order = new Order();
        order.setCustomerName(request.getRemoteUser());
        order.setOrderItems(orderitemService.getOrderItemList());
        System.out.println(">>>>>>>>>>>>>>> OrderItem" + orderitemService.getOrderItemList());
        orderService.save(order);
        return "redirect:/product/{id}/addtocart";
    }*/


    @PostMapping(value = "/product/{id}/details", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postAddtoCartPage(@Valid @RequestBody MultiValueMap<String, String> formData, HttpServletRequest request) {
        System.out.println(">>>>>>>>>>>Add to cart Product" + formData.getFirst("name"));
        request.getSession().setAttribute("name", formData.get("name").get(0));
        return "redirect:/product/{id}/addtocart";
    }


}
