package com.eieimon.project.demo.web;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eieimon.project.demo.entity.Category;
import com.eieimon.project.demo.entity.Order;
import com.eieimon.project.demo.entity.Product;
import com.eieimon.project.demo.entity.User;
import com.eieimon.project.demo.service.CategoryService;
import com.eieimon.project.demo.service.OrderService;
import com.eieimon.project.demo.service.ProductService;
import com.eieimon.project.demo.service.UserService;

@Controller
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    UserService userService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    OrderService orderService;

    @GetMapping("/products/create")
    public String productcreateForm(Model model) {
        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("product", new Product());
        return "createproductForm";
    }

    @PostMapping("/products/create")
    public String createUser(@ModelAttribute Product product, Model model, HttpServletRequest request,
            @RequestParam("category") Long categoryId) {
        System.out.println(">>>>>>>>>>>>>>>>>>>" + Calendar.getInstance().getTime());
        System.out.println(">>>>>>>>>>>>>>>>>>>" + categoryId);
        Category category = categoryService.findById(categoryId);
        product.setCategories(new HashSet<Category>());
        product.getCategories().add(category);
        User user = userService.findByEmail(request.getRemoteUser());
        product.setAdded_by(user);
        product.setCreated_on(Calendar.getInstance().getTime());
        productService.save(product);
        // model.addAttribute("id", product.getId());

        return "redirect:/productlist";
    }

    @GetMapping("/productlist")
    public String showProductLists(Model model) {
        model.addAttribute("productlist", productService.getProductList());
        return "productlist";
    }

    @GetMapping("/category/create")
    public String createCategoryForm(Model model) {
        model.addAttribute("category", new Category());
        return "createCategoryForm";
    }

    @PostMapping("/category/create")
    public String createCategory(@ModelAttribute Category category, Model model) {
        categoryService.save(category);
        return "redirect:/categorylist";
    }

    @GetMapping("/categorylist")
    public String showCategoryList(Model model) {
        model.addAttribute("categorylist", categoryService.findAll());
        return "categorylist";
    }

    @GetMapping("/api/products")
    @ResponseBody
    public List<Product> getProducts() {
        return productService.getProductList();
    }

    @GetMapping("/products/bycategory/{id}")
    public String showProductsByCategory(Model model, @PathVariable("id") Long id) {
        model.addAttribute("id", id);
        model.addAttribute("productsbycategorylist", categoryService.findByCategoryId(id));
        return "productsbycategorylist";
    }
    /*
     * @GetMapping("/products/{id}/details") public String showproductDetails(Model
     * model,@PathVariable("id") Long id) { model.addAttribute("id", id);
     * model.addAttribute("productdetails", productService.getProducts(id)); return
     * "productdetails"; }
     * 
     * @GetMapping("/product/{id}/order") public String showorderForm(Model
     * model,@PathVariable("id") Long id) { //model.addAttribute("id", id);
     * model.addAttribute("order",new Order()); return "orderForm"; }
     * 
     * @PostMapping("/product/{id}/order") public String
     * saveOrder(@Validated @ModelAttribute Order order,BindingResult
     * bindingResult,HttpServletRequest request,@PathVariable("id") Long id, Model
     * model) { Product products = productService.getProducts(id);
     * //order.setItem(products.getName());
     * System.out.println("Product Name>>>>>>>>>>>>>>" + products.getName());
     * //order.setPrice(products.getPrice());
     * order.setOrderdate(Calendar.getInstance().getTime());
     * order.setDelivery_address(order.getDelivery_address());
     * order.setStatus("Delivered"); // order.setUsername(request.getRemoteUser());
     * // orderService.save(order); return "redirect:/productlist";
     * 
     * }
     */
    
    
    
    @GetMapping("/product/{id}/addtocart")
    public String addToCart(Model model, @PathVariable("id") Long id, HttpServletRequest request) {
        model.addAttribute("id", id);
        
        Set<Long> productIds = (Set<Long>) request.getSession().getAttribute("cart");
        
        if (productIds == null) {
            productIds = new HashSet<Long>();
            productIds.add(id);
            request.getSession().setAttribute("cart", productIds);
        } else {
            productIds.add(id);
        }
        
        return "redirect:/product/{id}/details";
    }
    
    @GetMapping("/cart")
    public String showCart(HttpServletRequest request, Model model) {
        Set<Long> productIds = (Set<Long>) request.getSession().getAttribute("cart");
        
        List<Product> products = productService.getProductsByIds(productIds);
        model.addAttribute("productlist", products);
        return "cart";
    }
    
    @GetMapping("/order")
    public String showOrderForm(Model model) {
        model.addAttribute("order", new Order());
        return "orderform";
    }
    
    @PostMapping("/order")
    @ResponseBody
    public String order(@ModelAttribute Order order, Model model, HttpServletRequest request){
        Set<Long> productIds = (Set<Long>) request.getSession().getAttribute("cart");
        
        productService.saveOrder(order, productIds);
        
        return "order success";
        
    }
    

}
