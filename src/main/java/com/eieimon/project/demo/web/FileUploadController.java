package com.eieimon.project.demo.web;

import java.io.IOException;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.eieimon.project.demo.entity.Product;
import com.eieimon.project.demo.entity.ProductImage;
import com.eieimon.project.demo.service.ProductImageService;
import com.eieimon.project.demo.service.ProductService;
import com.eieimon.project.demo.service.StorageFileNotFoundException;
import com.eieimon.project.demo.service.StorageService;

@Controller
public class FileUploadController {

    private final StorageService storageService;
    @Autowired
    ProductService productService;
    @Autowired
    ProductImageService productImageService;
    ProductImage productImage = new ProductImage();

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/products/{id}/profile")
    public String uploadImage(Model model, @PathVariable("id") int id) throws IOException {
        model.addAttribute("files",
                storageService.loadAll()
                        .map(path -> MvcUriComponentsBuilder
                                .fromMethodName(FileUploadController.class, "serveFile", path.getFileName().toString())
                                .build().toString())
                        .collect(Collectors.toList()));
        model.addAttribute("id", id);

        return "uploadimageForm";
    }

    @GetMapping("/products/{id}/profile/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @PostMapping("products/{id}/profile")
    public String handleImageUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
            @PathVariable("id") Long productId) {
        String imageFileName = storageService.saveImageFile(file);
        Product product = productService.getProducts(productId);
        productImage.setProduct(product);
        productImage.setImage_path(imageFileName);
        productService.save(product);
        productImageService.saveImagePath(productImage);
        // String imageFileName = storageService.saveImageFile(file);
        // productImage.setImage_path(imageFileName);
        // productImage.setProduct(product);
        // productImageService.saveImagePath(productImage);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/productlist";
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound2(StorageFileNotFoundException exc) {

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/products/{id}/profileimg")
    @ResponseBody
    public ResponseEntity<Resource> profileImage(Model model, @PathVariable("id") long pid) throws IOException {
        ProductImage productImage = productImageService.findByProductId(pid);
        Resource file = storageService.loadAsResource(productImage.getImage_path());
        return ResponseEntity.ok().body(file);

    }

}
